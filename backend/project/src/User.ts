// import {MongoClient} from "mongodb";
import db from "./connections/db";
import { UserRegisterRequestModel, UserRegisterObjectModel } from './utilities/model/registerModel';
import { userModel } from './utilities/model/loginModel';




// const lol = require('../src/connections/MongoDBConnection').MyConnection;
// const ObjectId = require('mongodb').ObjectId;
// const log = require('./log');


class UserData {

    // convertToObjectId(id) {
    //     return ObjectId(id)
    // };
    connection: any
    constructor() {
        this.connection = db.getConnection()
    }

    async checkRegistration(filledData: UserRegisterRequestModel): Promise<boolean> {
        const response = await this.connection.collection('users').findOne({email: filledData.email.toLowerCase().trim()})
        return response != null
    };

    async getPassword(filledData: UserRegisterRequestModel): Promise<string> {
        const response = await this.connection.collection('users').findOne({email: filledData.email.toLowerCase().trim()})
        return response.password
    };

    async getUserData(filledData: UserRegisterRequestModel): Promise<userModel> {
        return await this.connection.collection('users').findOne({email: filledData.email.toLowerCase().trim()})
    };

    async getUserDataByEmail(email: string): Promise<userModel> {
        return await this.connection.collection('users').findOne({email: email})
    };

    async addNewUser (newUser: UserRegisterObjectModel) {
        try {
            return await this.connection.collection('users').insertOne(newUser);
            // log.info(`${user.email} was add successfully.`);
        } catch (err) {
            // log.error(`Failed to add user ${user.email}: ` + err);
            throw new Error(err)
        }
    }


}

module.exports = UserData;