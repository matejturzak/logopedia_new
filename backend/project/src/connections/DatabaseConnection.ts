import {MongoClient} from 'mongodb';


class DatabaseConnection {
    url: string;
    dbName: string;
    private client: MongoClient;
    private dbConnection: any = null;

    constructor(url: string, dbName: string, client: MongoClient){
        this.url = url;
        this.client = client;
        this.dbName = dbName;
        this.connectToMongoDB()
    }

    async connectToMongoDB () {
            try {
                this.client = new MongoClient(this.url, {useUnifiedTopology: true, connectTimeoutMS: 5000, serverSelectionTimeoutMS: 5000});
                await this.client.connect();
                this.dbConnection = this.client.db(this.dbName)
            } catch (err) {
                // log.error(`An error occurred while connecting to MongoDB ${branch} : ` + err);
                throw new Error(err);
            }
    }

    getConnection () {
        return this.dbConnection
    }
}

module.exports = DatabaseConnection;



