const DatabaseConnection = require("./DatabaseConnection")
const config = require ('../config');

const url = config.collection.url;
const dbName = config.collection.dbName;

export default new DatabaseConnection(url, dbName);