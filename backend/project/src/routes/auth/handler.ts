import express from "express";
const router = express.Router();

const tokenVerification = require('./tokenVerification');

router.get('/', () => {
})
    .use('/token-verification', tokenVerification)


module.exports = router;