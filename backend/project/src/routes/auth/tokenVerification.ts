import express from "express";
import {tokenObjectModel} from '../../utilities/model/tokenModel';
import {userModel} from "../../utilities/model/loginModel";
const decryptIdFromToken = require('../../utilities/tokenHandler').decryptIdFromToken;
const jwt = require('jsonwebtoken');
const authToken = require('../../keys').authToken;
const renewToken = require('../../keys').renewToken;
const UserData = require('../../User');
const tokenGenerator = require('../../utilities/tokenHandler').generateToken;
const router = express.Router();

router.post('/', async (req, res) => {
    const longExp : String | undefined = req.header('refreshToken');
    const shortExp: String | undefined = req.header('authToken');
    if (!shortExp || !longExp) {
        res.status(401).send();
        return
    }
    const User = new UserData();
    const previous = await decryptIdFromToken(shortExp);
    const userData: userModel = await User.getUserDataByEmail(previous.email);
    try {
        jwt.verify(shortExp, authToken);
        const newTokens: tokenObjectModel = await tokenGenerator(userData);
        res.status(200).send(newTokens);
        return
    } catch (err) {
        try {
            jwt.verify(longExp, renewToken);
            const newTokens: tokenObjectModel = await tokenGenerator(userData);
            res.status(200).send(newTokens)
            return
        } catch (err) {
            res.status(401).send();
            return
        }
    }

})

module.exports = router;
