import express from "express";
const router = express.Router();

const login = require('./login');
const register = require('./register');


router.get('/', () => {
})
    .use('/login', login)
    .use('/register', register)


module.exports = router;