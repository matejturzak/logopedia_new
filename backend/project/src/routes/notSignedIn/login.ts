import express from "express";
import {LoginRequestModel, userModel} from '../../utilities/model/loginModel';
import {tokenObjectModel} from '../../utilities/model/tokenModel';


const router = express.Router();
const UserLoginVerificator = require('../../utilities/user/UserLoginVerificator');
const ErrorMsg = require('../../utilities/ErrorMsg');
const UserData = require('../../User');
const tokenGenerator = require('../../utilities/tokenHandler').generateToken;


router.post('/', async (req, res) => {
    const filledData: LoginRequestModel = {email: req.body.email, password: req.body.password};
    const userVerificator = new UserLoginVerificator(filledData)
    const errorsFromValidation: Array<Object> = userVerificator.validationOfFiledData()
    if (errorsFromValidation.length > 0) {
        res.status(400).send(new ErrorMsg(errorsFromValidation))
        return
    }
    const User = new UserData();
    const isRegistered: boolean = await User.checkRegistration(filledData);
    if (!isRegistered) {
        res.status(400).send(new ErrorMsg("Filled incorrect credentials"))
        return
    }
    const currentPassword: string = await User.getPassword(filledData);
    const validPassword: boolean = await userVerificator.verifyPassword(currentPassword);
    if (!validPassword) {
        res.status(400).send(new ErrorMsg("Filled incorrect credentials"))
        return
    }
    try {
        const userData: userModel = await User.getUserData(filledData);
        const newTokens: tokenObjectModel = await tokenGenerator(userData);
        res.status(200).send(newTokens)
    } catch (err) {
        // log.error(`Failed to provide tokens to ${filledData.email}: ` + err);
        res.status(500).send(new ErrorMsg("Filled incorrect credentials"))
    }
})

module.exports = router;
