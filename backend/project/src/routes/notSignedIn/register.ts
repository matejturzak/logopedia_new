import express from "express";
import {UserRegisterRequestModel} from '../../utilities/model/registerModel'
const router = express.Router();
const UserRegistration = require('../../utilities/user/UserRegistrar');
const UserData = require('../../User');
const ErrorMessages = require('../../utilities/ErrorMessages');

const passwordHash = require('../../utilities/user/UserPasswordHash').hashPassword;


router.post('/', async (req, res) => {
    const filledData: UserRegisterRequestModel = {
        name: req.body.name,
        email: req.body.email,
        password1: req.body.password1,
        password2: req.body.password2
    };
    const user = new UserRegistration(filledData);
    const errorsFromValidation: Array<Object> = await user.credentialsValidation();
    if (errorsFromValidation.length > 0) {
        res.status(400).send(new ErrorMessages(errorsFromValidation))
        return
    }
    const User = new UserData();
    const isDuplicate: boolean = await User.checkRegistration(filledData);
    if (isDuplicate) {
        res.status(400).send(new ErrorMessages( ["User already exists"]))
        return
    }
    const newUser: Object = user.createUserObject();
    const userWithHashedPassword: Object = await passwordHash(newUser)
    try {
        await User.addNewUser(userWithHashedPassword);
        res.status(200).send()
    } catch (err) {
        // log.error(`Failed to add ${newUser.email} to current menu document and DB: ` + err);
        res.status(500).send(new ErrorMessages(["The issue appeared, the request cannot currently be processed."]))
    }
})


module.exports = router;
