const express = require('express');
const cors = require('cors');
const visitorRoutes = require('../routes/notSignedIn/handler');
const authRoutes = require('../routes/auth/handler');

const app = express();
app.use(express.json(), cors());
const port = 5001;

app.listen(port, () => {
    console.log(`App is listening on port ${port}!`)
});

app.use('/notSignedIn', visitorRoutes);
app.use('/auth', authRoutes);


// app.get('/test', (req:any,res:any) => {
//     const userData = db.lol();
//     console.log(1)
// })

module.exports = app;