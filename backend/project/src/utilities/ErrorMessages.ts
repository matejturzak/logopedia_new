class ErrorMessages {
    private errors: Array<string>

    constructor(errors: Array<string>) {
        this.errors = errors;
    }
}

module.exports = ErrorMessages;