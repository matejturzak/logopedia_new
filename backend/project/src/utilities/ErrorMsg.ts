class ErrorMsg {
    private error: String

    constructor(error: String) {
        this.error = error;
    }
}

module.exports = ErrorMsg;