export interface LoginRequestModel {
    email: string;
    password: string;
}

export interface userModel {
    name: string,
    email: string,
    password: string,
    role: string,
    verified: boolean,
    resetToken: string
}