export interface UserRegisterRequestModel {
    name: string;
    email: string;
    password1: string;
    password2: string;
}

export interface UserRegisterObjectModel {
    name: string,
    email: string,
    password: string,
    role: string,
    verified: boolean,
    resetToken: string
}
