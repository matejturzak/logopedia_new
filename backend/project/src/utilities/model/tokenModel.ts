export interface tokenObjectModel {
    refreshToken: string,
    authToken: string
}