const jwt = require('jsonwebtoken');
const authToken = require('../keys').authToken;
const renewToken = require('../keys').renewToken;
import {userModel} from './model/loginModel';
import {tokenObjectModel} from './model/tokenModel';

module.exports = {
    decryptIdFromToken: (token: String) => {
        return jwt.decode(token);
    },

    generateToken: (user: userModel) : tokenObjectModel => {
        return {
            authToken: jwt.sign({email: user.email,  admin: user.role === "admin", verified: user.verified}, authToken, {expiresIn: 30}),
            refreshToken: jwt.sign({email: user.email,  admin: user.role === "admin", verified: user.verified}, renewToken, {expiresIn: "7d"})
        }
    }


}