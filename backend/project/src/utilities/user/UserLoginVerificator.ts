const bcrypt = require('bcrypt');
import { LoginRequestModel } from '../model/loginModel';


class UserRegistrar {
    filledData: LoginRequestModel

    constructor(filledData: LoginRequestModel) {
        this.filledData = filledData;
    }

    validationOfFiledData () : Array<Object> {
        const errors: Array<Object> = [];
        const emailFormat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (!this.filledData.email || !this.filledData.password ) {
            errors.push({msg: "Please fill all fields"})
        }
        if (!this.filledData.email.match(emailFormat)) {
            errors.push({msg: "Email format is not valid"})
        }
        return errors
    }

    async verifyPassword(hashedPassword: string) : Promise<boolean> {
        const enteredPassword = this.filledData.password;
        return await bcrypt.compare(enteredPassword, hashedPassword);
    }


}

module.exports = UserRegistrar;