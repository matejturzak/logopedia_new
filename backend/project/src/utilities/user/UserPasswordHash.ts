const bcrypt = require('bcrypt');
import { UserRegisterObjectModel } from '../model/registerModel';


module.exports = {
    hashPassword: async (newUser: UserRegisterObjectModel) => {
        newUser.password = await bcrypt.hash(newUser.password, 10);
        return (newUser)
    },

}