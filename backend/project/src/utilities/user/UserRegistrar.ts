import { UserRegisterRequestModel, UserRegisterObjectModel } from '../model/registerModel';

class UserRegistrar {
    filledData: UserRegisterRequestModel

    constructor(filledData: UserRegisterRequestModel) {
        this.filledData = filledData;
    }

    credentialsValidation (): Array<Object> {
        const errors: Array<Object> = [];
        const emailFormat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (!this.filledData.name || !this.filledData.email || !this.filledData.password1 || !this.filledData.password2) {
            errors.push({msg: "Please fill all fields"})
        }
        if (this.filledData.password1 !== this.filledData.password2) {
            errors.push({msg: "Passwords do not match"})
        }
        if (this.filledData.password1.length < 6) {
            errors.push({msg: "Password should be at least 6 characters"})
        }
        if (this.filledData.email && !this.filledData.email.match(emailFormat)) {
            errors.push({msg: "Email format is not valid"})
        }
        return errors
    }


    createUserObject (): UserRegisterObjectModel {
        return {
            name: this.filledData.name.trim(),
            email: this.filledData.email.trim(),
            password: this.filledData.password1.trim(),
            role: "user",
            verified: false,
            resetToken: ""
        }
    }
}

module.exports = UserRegistrar;