import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import LogIn from "../components/visitor/LogIn";
import SignUp from "../components/visitor/SignUp";
import PasswordReset from "../components/visitor/password/PasswordResetRequest";
import NewPassword from "../components/visitor/password/Password";
import ProtectedRoutes from "../components/authenticated/ProtectedRoutes";
import ProtectedAdminRoutes from "../components/authenticated/ProtectedAdminRoutes";
import Dashboard from "../components/authenticated/user/Dashboard";
import AdminDashboard from "../components/authenticated/admin/AdminDashboard";
import Navigation from "../components/authenticated/navigation/Navigation";
import NotProtectedRoutes from "../components/visitor/NotProtectedRoutes";




export class Routes extends React.Component {
    render() {
        return (
                <Switch>
                    <NotProtectedRoutes exact path="/log-in" component={LogIn}/>
                    <NotProtectedRoutes exact path="/sign-up" component={SignUp}/>
                    <NotProtectedRoutes exact path="/password-reset" component={PasswordReset}/>
                    <NotProtectedRoutes exact path="/password-reset/:token" component={NewPassword}/>
                    {/*<Navigation>*/}
                    <ProtectedRoutes exact path="/auth/dashboard" component={Dashboard}/>
                    <ProtectedAdminRoutes exact path="/auth/admin" component={AdminDashboard}/>
                    {/*</Navigation>*/}
                </Switch>
        )
    }
}

export default Routes;