import Sync from "./Sync";
import ErrorMsg from "./ErrorMsg"
import ErrorMessages from "./ErrorMessages"


const path = "http://localhost:5001";

class Auth {
    constructor() {
        this.newSync = new Sync();
    }

    async logIn(email, password) {
        const url = path + "/notSignedIn/login";
        const payload = {
            "email": email,
            "password": password
        };
        const response = await this.newSync.post(url, payload);
        if (response.status !== 200) {
            throw new ErrorMsg(response.message.error)
        }
    };

    async handleRegistration(name, email, password1, password2) {
        const payload = {
            "name": name,
            "email": email,
            "password1": password1,
            "password2": password2,
        };
        const url = path + "/notSignedIn/register";
        const response = await this.newSync.post(url, payload);
        if (response.status !== 200) {
            throw new ErrorMessages(response.message.errors)
        }
    };

    async checkAuthentication() {
        const localToken = localStorage.getItem('authToken');
        if (localToken === null) {
            new Error("Not authenticated")
        } else {
            const url = path + "/auth/token-verification";
            const response = await this.newSync.post(url, null);
            if (response.status !== 200) {
                throw new ErrorMsg("Not authenticated")
            }
        }
    };


    //
    // async handlePasswordReset(payload) {
    //     const url = path + "/users/reset-password";
    //     return await this.newSync.post(url, payload);
    // };
    //
    // async handleOfNewPasswordSubmit(payload) {
    //     const url = path + "/users/reset-password-submit";
    //     return await this.newSync.post(url, payload);
    // };
    //
    // async verifyResetLink(payload) {
    //     const url = path + "/users/reset-password-verify";
    //     return await this.newSync.post(url, payload);
    // };
    //
    // async getUsers() {
    //     const url = path + "/auth/api/admin/";
    //     return await this.newSync.get(url);
    // };
    //
    // async handleUserDelete(userToBeRemoved) {
    //     const url = path + "/auth/api/admin/";
    //     const payload = {toBeDeletedUniqueUserId: userToBeRemoved._id, menuUserId: userToBeRemoved.id};
    //     return await this.newSync.delete(url, payload);
    // };
    //
    // async getMenuData(id) {
    //     const payload = {"id": id};
    //     const url = path + `/auth/api/user/menu/`;
    //     return await this.newSync.post(url, payload);
    // };
    //
    // async getMenuDataAdmin(id) {
    //     const payload = {"entity": id};
    //     const url = path + `/auth/api/admin/menu/`;
    //     return await this.newSync.post(url, payload);
    // };
    //
    // async uploadMenu(payload, role) {
    //     const url = path + `/auth/api/${role}/menuUpload/`;
    //     return await this.newSync.post(url, payload);
    // };
    //
    // async handleUserUpdate(dataToBeUpdated) {
    //     const url = path + "/auth/api/admin/";
    //     return await this.newSync.post(url, dataToBeUpdated);
    // };
    //
    // async handleForceUpdate() {
    //     const url = path + "/auth/api/admin/test/";
    //     return await this.newSync.post(url, null);
    // }

}

export default Auth;