import axios from "axios";

class Sync {
    constructor() {
        this.headers = {
            'authToken': localStorage.getItem('authToken'),
            'refreshToken': localStorage.getItem('refreshToken'),
        };
    }

    async post(url, payload) {
        try {
            const res = await axios.post(url, payload, {headers: this.headers});
            return await this.handleResponse(res);
        } catch (err) {
            console.log(err)
            return await this.handleErrorResponse(err.response);
        }
    };

    // async get(url) {
    //     try {
    //         const res = await axios.get(url, {headers: this.headers});
    //         return await this.handleResponse(res);
    //     } catch (err) {
    //         return await this.handleErrorResponse(err.response);
    //     }
    // };
    //
    // async delete(url, payload) {
    //     try {
    //         return await axios.delete(url, {data: payload, headers: this.headers});
    //     } catch (err) {
    //         return this.handleErrorResponse(err.response.status);
    //     }
    // };

    handleErrorResponse(response) {
        if (response.status === 400) {
            return {status: 400, message: response?.data ? response.data : null }
        } else if (response.status === 401 || response.status === 403) {
            localStorage.removeItem("authToken");
            localStorage.removeItem("refreshToken");
            return {status: 401, message: "User is not currently logged in."}
        } else if (response.status === 404) {
            return {status: 404, message: "Task could not be completed."}
        } else if (response.status === 409 ) {
            return {status: 409, message: response?.data ? response?.data : "Task could not be completed, please perform action again."}
        } else if (response.status === 410 ) {
            return {status: 410, message: "Expired"}
        } else if (response.status === 500) {
            const msg = "Issue appeared, the request cannot currently be processed.";
            alert(msg);
            return {status: 500, message: msg}
        } else {

        }
    };

    async handleResponse(response) {
        if (response.status < 304 && response.data?.authToken) {
            await window.localStorage.setItem('authToken', response.data.authToken);
            await window.localStorage.setItem('refreshToken', response.data.refreshToken);
            return {status: response.status, data: response?.data ? response.data : null};
        } else {
            return {status: response.status, data: response?.data ? response.data : null};
        }
    }


}

export default Sync;