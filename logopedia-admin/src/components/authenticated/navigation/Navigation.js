import React from 'react';
import {withRouter} from "react-router-dom";
import TopBar from "./Topbar";
import Sidebar from "./Sidebar";

class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openSidebar: false,
        };
    }

    handleSidebarOpen = () => {
        this.setState({ openSidebar: true });
    };

    handleSidebarClose = () => {
        this.setState({ openSidebar: false });
    };

    handleLogOut = () => {
        localStorage.removeItem("auth-token");
        localStorage.removeItem("refresh-token");
        window.location = '/log-in';
    };

    render() {
        return (
            <React.Fragment>
                <TopBar onSidebarOpen={this.handleSidebarOpen} onLogOut={this.handleLogOut} />
                <Sidebar
                    open={this.state.openSidebar}
                    onOpen={this.handleSidebarOpen}
                    onClose={this.handleSidebarClose}
                    person={this.props.person}
                />
            </React.Fragment>
        )}

}

export default withRouter(Navigation)
