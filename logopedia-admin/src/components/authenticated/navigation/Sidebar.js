import React, { Component } from "react";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import { withStyles } from "@material-ui/core";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FastFoodIcon from '@material-ui/icons/Fastfood';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import SettingsIcon from '@material-ui/icons/Settings';
import { withRouter } from "react-router-dom";
import DashboardIcon from '@material-ui/icons/Dashboard';


const styles = {
    fullList: {
        width: 300
    }
};

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openSidebar: false,
            admin: false
        };
    }

    fullList = () => {
        const { classes, onClose } = this.props;
        return (
            <div
                className={classes.fullList}
                role="presentation"
                onClick={onClose}
                onKeyDown={onClose}
            >
                <List>
                    <ListItem button key="Dashboard" onClick={() => {this.props.history.push("/auth/dashboard")}}>
                        <ListItemIcon>
                            <DashboardIcon />
                        </ListItemIcon>
                        <ListItemText primary="Dashboard"/>
                    </ListItem>
                    <ListItem button key="Menu uploader" onClick={() => {this.props.history.push("/auth/menu/0")}}>
                        <ListItemIcon>
                            <FastFoodIcon />
                        </ListItemIcon>
                        <ListItemText primary="Menu uploader"/>
                    </ListItem>
                    <Divider />
                    <ListItem button key="Administration"  onClick={() => {this.props.history.push("/auth/admin")}}>
                        <ListItemIcon>
                            <SupervisorAccountIcon />
                        </ListItemIcon>
                        <ListItemText primary="Administration"/>
                    </ListItem>
                    <ListItem button key="Settings" onClick={() => {this.props.history.push("/auth/settings")}}>
                        <ListItemIcon>
                            <SettingsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Settings"/>
                    </ListItem>
                </List>
            </div>
        );
    };



    render() {
        const { open, onOpen, onClose } = this.props;
        return (
            <SwipeableDrawer
                open={open}
                onOpen={onOpen}
                onClose={onClose}
            >
                {this.fullList()}
            </SwipeableDrawer>
        );
    }
}

export default withRouter(withStyles(styles)(Sidebar));
