import React, {Component} from "react";
import {AppBar, IconButton, Toolbar, Typography,} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import Button from "@material-ui/core/Button";


class TopBar extends Component {
    render() {
        const {onSidebarOpen, onLogOut} = this.props;
        return (
            <div>
                <AppBar position="static" className="appBar">
                    <Toolbar>
                        <IconButton edge="start" color="inherit" aria-label="menu" onClick={onSidebarOpen}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" className="navBarAlign">
                            CampusFood
                        </Typography>
                        <Button color="inherit" onClick={onLogOut}>Logout</Button>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}


export default withRouter(TopBar);
