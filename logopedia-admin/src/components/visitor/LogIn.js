import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockIcon from '@material-ui/icons/Lock';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
// import "../../styles/style.scss";
import {createMuiTheme, ThemeProvider} from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import {connect} from "react-redux";
import Alert from "@material-ui/lab/Alert";
import {loginActions} from "../../redux/action/loginActions";
import {tokenAction} from "../../redux/action/tokenAction";


class LogIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleProvidedCredential = this.handleProvidedCredential.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    };

    async handleProvidedCredential(e) {
        let name = e.target.name;
        let value = e.target.value;
        this.props.filledCredential(name, value)
    };


    onSubmit(e) {
        e.preventDefault();
        const {email, password} = this.props.credentials;
        if (email && password) {
            this.props.login(email, password)
        }
    };


    render() {
        const theme = createMuiTheme({
            palette: {
                primary: {main: "#807b82"},
                secondary: {main: "#1272c4"},
                button: {main: "#1e9c2a"}
            }
        });
        return (
                <div>
                    <Container component="main" maxWidth="xs">
                        <CssBaseline/>
                        <ThemeProvider theme={theme}>
                            <div className='head-top'>
                                <Avatar className='user-avatar'>
                                    <LockIcon/>
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Sign in
                                </Typography>
                                {(this.props.error !== "") ?
                                    <Alert severity="warning" style={{width: '100%'}}>Email or password is
                                        wrong.</Alert> :
                                    null
                                }
                                {(this.props.newRegistration === true) ?
                                    <Alert severity="success" style={{width: '100%'}}>Successfully registered.</Alert> :
                                    null
                                }
                                <form noValidate className='form'>
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="email"
                                        label="Email Address"
                                        name="email"
                                        autoComplete="email"
                                        autoFocus
                                        onChange={e => this.handleProvidedCredential(e)}
                                    />
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                        onChange={e => this.handleProvidedCredential(e)}
                                    />
                                    <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        onClick={this.onSubmit}
                                    >
                                        Sign In
                                    </Button>
                                    <Grid container>
                                        <Grid item xs>
                                            <Link href="/sign-up" variant="body2" color="secondary">
                                                {"Don't have an account? Sign Up"}
                                            </Link>
                                        </Grid>
                                        <Grid item>
                                            <Link href="/password-reset" variant="body2" color="secondary">
                                                {"Forgot password?"}
                                            </Link>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </ThemeProvider>
                        <Box mt={8}>
                        </Box>
                    </Container>
                </div>
        )

    }
}

const mapStateToProps = state => ({
    credentials: state.visitorLoginReducer.credentials,
    error: state.visitorLoginReducer.error,
    newRegistration: state.visitorRegisterReducer.newRegistration,
    checking: state.authReducer.checking,
    verified: state.authReducer.verified,
    checked: state.authReducer.checked
})


const mapDispatchToProps = {
    verifyToken: tokenAction.verifyToken,
    filledCredential: loginActions.filledCredential,
    login: loginActions.login,
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps)(LogIn));