import React from 'react';
import {Route, Redirect} from "react-router-dom";
import {connect} from "react-redux"
import {tokenAction} from "../../redux/action/tokenAction";
import {withRouter} from "react-router-dom";


class NotProtectedRoutes extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {}
        this.props.verifyToken()
    }


    render() {
        const Component = this.props.component;
        return !this.props.checked || this.props.checking ? null :
            !this.props.verified ?
                <Route path={this.props.path} children={<Component/>}/>
                :
                <Redirect to={{pathname: '/auth/dashboard', state: {from: this.props.location}}}/>

    }

}


const mapDispatchToProps = {
    verifyToken: tokenAction.verifyToken
}

const mapStateToProps = state => ({
    checking: state.authReducer.checking,
    verified: state.authReducer.verified,
    checked: state.authReducer.checked,
    userVerified: state.authReducer.userVerified
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps)(NotProtectedRoutes));
