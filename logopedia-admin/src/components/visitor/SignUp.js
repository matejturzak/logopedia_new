import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import LockIcon from '@material-ui/icons/Lock';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {ThemeProvider} from '@material-ui/core/styles';
import {createMuiTheme} from '@material-ui/core/styles';
// import "../../styles/style.scss";
import {Redirect, Route, withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {registerActions} from "../../redux/action/registerActions";
import {tokenAction} from "../../redux/action/tokenAction";


class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleProvidedRegistrationValue = this.handleProvidedRegistrationValue.bind(this);
        this.onRegister = this.onRegister.bind(this);
    }

    async handleProvidedRegistrationValue(e) {
        let name = e.target.name;
        let value = e.target.value;
        this.props.fillingRegistrationForm(name, value)
    }


    onRegister(e) {
        e.preventDefault();
        const {name, email, password1, password2} = this.props.registerData;
        if (name && email && password1 && password2) {
            this.props.register(name, email, password1, password2)
        }
    }

    render() {
        const theme = createMuiTheme({
            palette: {
                primary: {main: "#807b82"},
                secondary: {main: "#1272c4"},
            }
        });
        return (
                <div>
                    <Container component="main" maxWidth="xs">
                        <CssBaseline/>
                        <ThemeProvider theme={theme}>
                            <div className='head-top'>
                                <Avatar className='user-avatar'>
                                    <LockIcon/>
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Register
                                </Typography>
                                {(this.props.errors.length > 0) ?
                                    this.props.errors.map((error, key) => {
                                        return (
                                            <Alert severity="warning" key={key}
                                                   style={{width: '100%'}}>{error.msg}</Alert>)
                                    })
                                    : null
                                }
                                <form noValidate className='form'>
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="register-name"
                                        label="Register name"
                                        name="name"
                                        autoComplete="name"
                                        autoFocus
                                        color="primary"
                                        onChange={e => this.handleProvidedRegistrationValue(e)}
                                    />
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="email"
                                        label="Email"
                                        type="email"
                                        id="email"
                                        onChange={e => this.handleProvidedRegistrationValue(e)}
                                    />
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="password1"
                                        label="Password"
                                        type="password"
                                        id="password1"
                                        onChange={e => this.handleProvidedRegistrationValue(e)}
                                    />
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="password2"
                                        label="Password verification"
                                        type="password"
                                        id="password2"
                                        onChange={e => this.handleProvidedRegistrationValue(e)}
                                    />
                                    <div className="submit-button">
                                        <Button
                                            type="submit"
                                            fullWidth
                                            variant="contained"
                                            onClick={this.onRegister}
                                        >
                                            Register
                                        </Button>
                                        <Grid container justify="flex-end">
                                            <Grid item>
                                                <Link href="/log-in" variant="body2" color="secondary">
                                                    Already have an account? Sign in
                                                </Link>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </form>
                            </div>
                        </ThemeProvider>
                        <Box mt={8}>
                        </Box>
                    </Container>
                </div>
        )
    }
}


const mapDispatchToProps = {
    fillingRegistrationForm: registerActions.fillingRegistrationForm,
    register: registerActions.register,
    verifyToken: tokenAction.verifyToken
}

const mapStateToProps = state => ({
    registerData: state.visitorRegisterReducer.registerData,
    errors: state.visitorRegisterReducer.errors,
    checking: state.authReducer.checking,
    verified: state.authReducer.verified,
    checked: state.authReducer.checked,

})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps)(SignUp));