import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import * as serviceWorker from './serviceWorker';
import store from './redux/store'
// import { ConnectedRouter } from 'connected-react-router';
// import * as lol from './history'
import history from "./history";
import {Router} from "react-router-dom";


ReactDOM.render(
    <Router history={history}>
        <Provider store={store}>
            <App/>
        </Provider>
    </Router>,

    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
