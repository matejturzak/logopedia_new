import {userAnonymousConstants} from "./actionTypes";
import Auth from "../../Sync/Auth";
import history from '../../history'


export const loginActions = {
    filledCredential: (name, value) => ({
        type: userAnonymousConstants.FILLED_CREDENTIAL,
        payload: {
            name: name,
            value: value
        }
    }),
    login: (email, password) => {
        return async (dispatch) => {
            dispatch(request());
            const auth = new Auth();
            try {
                await auth.logIn(email, password)
                await dispatch(success())
                history.push('/auth/dashboard');
            } catch (err) {
                dispatch(failure(err.error));
            };
        };

        function request() {
            return {type: userAnonymousConstants.LOGIN_REQUEST}
        }

        function success() {
            return {type: userAnonymousConstants.LOGIN_SUCCESS}
        }

        function failure(error) {
            console.log(error)
            return {type: userAnonymousConstants.LOGIN_FAILURE, error}
        }
    }
}
