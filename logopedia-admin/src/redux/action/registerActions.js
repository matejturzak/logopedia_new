import {userAnonymousConstants} from "./actionTypes";
import Auth from "../../Sync/Auth";
import history from '../../history'


export const registerActions = {
    fillingRegistrationForm: (name, value) => ({
        type: userAnonymousConstants.FILLED_REGISTER_DATA,
        payload: {
            name: name,
            value: value
        }
    }),
    register: (name, email, password1, password2) => {
        return async (dispatch) => {
            dispatch(request());
            const auth = new Auth();
            try {
                await auth.handleRegistration(name, email, password1, password2)
                await dispatch(success())
                history.push('/log-in');
            } catch (err) {
                dispatch(failure(err.errors));
            };
        };

        function request() {
            return {type: userAnonymousConstants.REGISTER_REQUEST}
        }

        function success() {
            return {type: userAnonymousConstants.REGISTER_SUCCESS}
        }

        function failure(errors) {
            return {type: userAnonymousConstants.REGISTER_FAILURE, errors}
        }
    },
}
