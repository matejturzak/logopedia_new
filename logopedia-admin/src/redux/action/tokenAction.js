import {authConstants} from "./actionTypes";
import Auth from "../../Sync/Auth";
import jwt_decode from 'jwt-decode'


export const tokenAction = {
    verifyToken: () => {
        return (dispatch) => {
            dispatch(request());
            const auth = new Auth();
            try {
                auth.checkAuthentication();
                const token = window.localStorage.getItem("refreshToken");
                const user = jwt_decode(token)
                dispatch(success(user.verified, user.admin))
            } catch (err) {
                dispatch(failure());
            };
        };

        function request() {
            return {type: authConstants.TOKEN_REQUEST}
        }

        function success(verified, admin) {
            return {type: authConstants.TOKEN_SUCCESS, userVerified: verified, admin: admin}
        }

        function failure() {
            return {type: authConstants.TOKEN_FAILURE}
        }
    }
}
