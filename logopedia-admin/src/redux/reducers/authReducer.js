import { authConstants } from "../action/actionTypes";


const initialState = {
    checked: false,
    verified: false,
    checking: false,
    admin: false,
    userVerified: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case authConstants.TOKEN_REQUEST:
            return {
                ...state,
                checking: true,
            }
        case authConstants.TOKEN_SUCCESS:
            return {
                ...state,
                checked: true,
                verified: true,
                checking: false,
                admin: action.admin,
                userVerified: action.userVerified
            }
        case authConstants.TOKEN_FAILURE:
            return {
                ...state,
                checked: true,
                verified: false,
                checking: false,
            }
        default:
            return state
    }
}

