import {combineReducers} from 'redux';
import visitorLoginReducer from './visitorLoginReducer';
import visitorRegisterReducer from './visitorRegisterReducer';
import authReducer from './authReducer';




export default combineReducers({visitorLoginReducer,visitorRegisterReducer, authReducer});
