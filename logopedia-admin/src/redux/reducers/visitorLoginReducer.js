import { userAnonymousConstants } from "../action/actionTypes";
import update from 'immutability-helper';


const initialState = {
    credentials: {
        email: "",
        password: ""
    },
    loggingIn: false,
    loggedIn: false,
    error: "",
};

export default function (state = initialState, action) {
    switch (action.type) {
        case userAnonymousConstants.FILLED_CREDENTIAL:
            return update(state, {
                credentials: {
                    [action.payload.name]: {$set: action.payload.value}
                }
            });
        case userAnonymousConstants.LOGIN_REQUEST:
            return {
                ...state,
                loggingIn: true
            }
        case userAnonymousConstants.LOGIN_SUCCESS:
            return {
                ...state,
                loggedIn: true,
                error: ""
            }
        case userAnonymousConstants.LOGIN_FAILURE:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}

