import { userAnonymousConstants } from "../action/actionTypes";
import update from 'immutability-helper';


const initialState = {
    registerData: {
        name: "",
        email: "",
        password1: "",
        password2: "",
    },
    registering: false,
    newRegistration: false,
    errors: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case userAnonymousConstants.FILLED_REGISTER_DATA:
            return update(state, {
                registerData: {
                    [action.payload.name]: {$set: action.payload.value}
                }
            });
        case userAnonymousConstants.REGISTER_REQUEST:
            return {
                ...state,
                registering: true
            }
        case userAnonymousConstants.REGISTER_SUCCESS:
            return {
                ...state,
                newRegistration: true,
                errors: []
            }
        case userAnonymousConstants.REGISTER_FAILURE:
            console.log(action.errors)
            return {
                ...state,
                errors: action.errors
            }

        default:
            return state
    }
}

