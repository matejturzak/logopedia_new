import { createStore, compose, applyMiddleware } from "redux";
// import { routerMiddleware } from 'connected-react-router';
import rootReducer from "./reducers";
import ReduxThunk from 'redux-thunk';



const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    composeEnhancer(applyMiddleware(ReduxThunk)),
);

export default store;

